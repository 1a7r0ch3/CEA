<table><tr>
<td width="50%"><img src="screenshots/beamerCEA-0.svg" width="100%"/></td>
<td width="50%"><img src="screenshots/beamerCEA-2.svg" width="100%"/></td>
</tr><tr>
<td width="50%"><img src="screenshots/beamerCEA-3.svg" width="100%"/></td>
<td width="50%"><img src="screenshots/beamerCEA-5.svg" width="100%"/></td>
</tr></table>

<table><tr valign="top"><td>

## Thème du Commissariat à l'énergie atomique pour diaporamas et posters avec LaTeX

Le thème est defini dans `beamerthemeCEA.sty`.  
Le modèle d'utilisation est `beamerCEA.ltx`.   
Les images nécessaires sont dans le dossier `graphics/`.  

### Polices d'écriture
La charte graphique du CEA utilise la police d'écriture Helvetica, chargée avec le paquet `helvet`. Pour la police d'écriture mathématique je préfère aussi utiliser Helvetica, pour une transition plus agréable avec le texte.  
Les lettres grecques et les symboles sont empruntés à la police Computer Modern sans sérif.  
Les majuscules ajourées utilisent la police Doublestroke sans sérif.  
Les lettres calligraphiques utilisent la police ITC Zapf Chancery, mise à l'échelle avec le paquet `mathalfa`.  

### Divers
Le paquet `babel` avec l'option `french` n'est pas chargé par défaut dans le thème, mais est inclus dans le modèle.  
Le modèle inclue plusieurs commandes classiques dans le préambule, et les logotypes peuvent être aussi modifiés à cet endroit. En particulier, deux alternatives sont proposées à titre d'exemple, l'une adaptée à la direction de l'énergie nucléaire, l'autre adaptée à l'institut de recherche sur la fusion par confinement magnétique.  

</td><td>

## CEA theme for beamers and posters with LaTeX
Theme is defined in `beamerthemeCEA.sty`.  
Template is `beamerCEA.ltx`.  
Required graphics are in `graphics/`.  

### Typefaces
CEA uses the Helvetica typeface, loaded with package `helvet`.
For the math typeface, I prefer to use also Helvetica, for a better blending with the text.  
Greeks and symbols are taken from the sans-serif Computer Modern typeface.  
Blackboard letters are set to sans-serif Doublestroke typeface.  
Calligraphic letters are set as Zapf Chancery and scaled with the `mathalfa` package.  

### Misc
The `babel` package with option `french` is not loaded by default in the theme, but included in the template.  
The template includes several classical commands in the preambule; logotypes can be changed here as well. In particular, two different possibilities are given as examples, one adapted to the DEN department, the other to the IRFM laboratory.

</td></tr></table>
